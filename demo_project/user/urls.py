from django.contrib.auth import views
from django.urls import path

from .views import (
    register,
    UserProfileDetailView,
    UserProfileUpdateView,
    UserPasswordChangeView
)


urlpatterns = [
    path('login/', views.LoginView.as_view(template_name='user/login.html'), name='user-login'),
    path('logout/', views.LogoutView.as_view(template_name='user/logout.html'), name='user-logout'),
    path('password_change/', UserPasswordChangeView.as_view(), name='user-password-change'),
    path('register/', register, name='user-register'),
    path('<int:pk>/profile/', UserProfileDetailView.as_view(), name='user-profile'),
    path('<int:pk>/profile/edit/', UserProfileUpdateView.as_view(), name='user-profile-edit')
]
