from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.views import PasswordChangeView
from django.views.generic import TemplateView, DetailView, UpdateView
from django.urls import reverse_lazy

from .models import Profile


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('user-login')
    else:
        form = UserCreationForm()

    return render(request, 'user/register.html', {'form': form})


class UserProfileDetailView(DetailView):
    model = Profile


class UserProfileUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Profile
    fields = ['image', 'info']

    def test_func(self):
        profile_user = self.get_object().user
        if self.request.user == profile_user:
            return True
        return False


class UserPasswordChangeView(PasswordChangeView):
    template_name = 'user/password_change_form.html'
    success_url = reverse_lazy('blog-home')
